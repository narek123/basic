<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;

class languageSwitcher extends Widget
{
    /**
     * @var string
     */
    public $default_lang="en";
    /**
     * @var
     */
    public $language;
    /**
     *
     * @var array
     */
    protected static $languages = [
       'en' => 'En',
        'am' => 'Am',
    ];

    /**
     * @return bool
     */
    public function init()
    {
        if (php_sapi_name() === 'cli') {
            return true;
        }

        parent::init();

        $session = Yii::$app->session;
        $languageNew = Yii::$app->request->get('language');
        if ($languageNew) {
            if (isset(self::$languages[$languageNew])) {
                Yii::$app->language = $languageNew;
                $session->set('language', $languageNew);
            }
        } elseif ($session->has('language')) {

            Yii::$app->language = $session->get('language');
        }
        else{
            $languageNew=$this->default_lang;
            Yii::$app->language = $languageNew;
            $session->set('language', $languageNew);
        }


    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $languages = self::$languages;
        $current = $languages[Yii::$app->language];
        unset($languages[Yii::$app->language]);

        $items = [];
        foreach ($languages as $code => $language) {
            $temp = [];
            $temp['label'] = $language;
            $temp['class'] = 'switch';
            $temp['url'] = Url::current(['language' => $code]);
            array_push($items, $temp);
        }
        echo ButtonDropdown::widget([
            'id'=>'switch',
            'options'=>[
                'id' => 'lang-switch',
                'class'=>'navbar-right',
            ],
            'label' => $current,
            'dropdown' => [
                'items' => $items,
            ],
        ]);
    }

    /**
     * @return array|mixed
     */
    public static function getLanguage()
    {
        if(Yii::$app->request->get('language')){
            $languages=array_flip(self::$languages);
            $newLanguage=Yii::$app->request->get('language');
            if(in_array($newLanguage,$languages)){
                Yii::$app->language=$newLanguage;
               return Yii::$app->request->get('language');
            }else{
                Yii::$app->language = Yii::$app->session->get('language');
                return Yii::$app->session->get('language');
            }
        }
        else{
            Yii::$app->language = Yii::$app->session->get('language');
            return Yii::$app->session->get('language');
        }
    }

    public static function Languages(){
        $newArray=[];
        foreach(self::$languages as $value){
            $newArray[]=$value;
        }
        return $newArray;
    }
}