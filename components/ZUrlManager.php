<?php
/**
 * Created by PhpStorm.
 * User: Nar
 * Date: 6/4/2016
 * Time: 22:27
 */

namespace app\components;

use yii\web\UrlManager;
use Yii;

class ZUrlManager extends UrlManager
{

    public function createUrl($params)
    {

        if (!isset($params['language'])) {
            if (Yii::$app->session->has('language')) {
                Yii::$app->language = Yii::$app->session->get('language');
            } else if (isset(Yii::$app->request->cookies['language'])) {
                Yii::$app->language = Yii::$app->request->cookies['language']->value;
            }
            $params[0] = languageSwitcher::getLanguage() . '/' . $params[0];
//            $params['language']=languageSwitcher::getLanguage();
        }
        $contr =explode('/',$params[0]);
        if(in_array('gii',$contr) || in_array('debug',$contr)){
           unset($contr[0]);
            $url=implode('/',$contr);
            $params[0]=$url;
        }
        return parent::createUrl($params);
    }
}