<?php
/**
 * Created by PhpStorm.
 * User: Nar
 * Date: 6/22/2016
 * Time: 14:50
 */

namespace app\components;


use yii\web\Controller;

class ZController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->language = languageSwitcher::getLanguage();
        \Yii::$app->session->set('language', languageSwitcher::getLanguage());
    }
}