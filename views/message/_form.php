<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
if($model->id){
    $lang = array();
    if (empty($model->getAvailableLang($model->id))) {
        $lang[$model->language] = $model->language;
    } else {
        $lang = $model->getAvailableLang($model->id);
    }
}else{
    $lang=['en'=>'en','am'=>'am'];
}

?>
<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->dropDownList(
        $model->getMessages(),
        ['prompt' => 'Select Message'])->label('Message');
    ?>
    <?= $form->field($model, 'language')->dropDownList(
        $lang,
        ['prompt' => 'Select Message']); ?>

    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
