<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends \yii\db\ActiveRecord
{
    const Languages = ['en', 'am'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceMessage::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('yii', 'ID'),
            'language' => Yii::t('yii', 'Language'),
            'translation' => Yii::t('yii', 'Translation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }

    public function getMessages()
    {
        $source = SourceMessage::find()->asArray()->all();
        return ArrayHelper::map($source, 'id','message');
    }

    public function getAvailableLang($id)
    {
       $lang = Message::findBySql('SELECT `language` FROM `message` WHERE `id` = '.$id)->asArray()->all();
        $l = array();
        foreach ($lang as $val){
            $l[]=$val['language'];
        }
        $res = array_diff(self::Languages,$l);
        return $res;
    }
}
